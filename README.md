# alpine-novnc

#### [alpine-x64-novnc](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-novnc/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64/alpine-x64-novnc.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-novnc "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64/alpine-x64-novnc.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-novnc "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-novnc.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-novnc/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-novnc.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-novnc/)
#### [alpine-aarch64-novnc](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-novnc/)
[![](https://images.microbadger.com/badges/version/forumi0721alpineaarch64/alpine-aarch64-novnc.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-novnc "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpineaarch64/alpine-aarch64-novnc.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-novnc "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64/alpine-aarch64-novnc.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-novnc/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64/alpine-aarch64-novnc.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-novnc/)
#### [alpine-armhf-novnc](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-novnc/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinearmhf/alpine-armhf-novnc.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-novnc "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinearmhf/alpine-armhf-novnc.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-novnc "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhf/alpine-armhf-novnc.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-novnc/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhf/alpine-armhf-novnc.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-novnc/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [noVNC](https://novnc.com/)
    - VNC client using HTML5 (WebSockets, Canvas) 
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)
    - [forumi0721/alpine-aarch64-base](https://hub.docker.com/r/forumi0721/alpine-aarch64-base/)
    - [forumi0721/alpine-armhf-base](https://hub.docker.com/r/forumi0721/alpine-armhf-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -d \
           -p 6080:0680/tcp \
           -e VNC_HOST=localhost \
           -e VNC_PORT=5900 \
           forumi0721alpinex64/alpine-x64-novnc:latest
```

* aarch64
```sh
docker run -d \
           -p 6080:0680/tcp \
           -e VNC_HOST=localhost \
           -e VNC_PORT=5900 \
           forumi0721alpineaarch64/alpine-aarch64-novnc:latest
```

* armhf
```sh
docker run -d \
           -p 6080:0680/tcp \
           -e VNC_HOST=localhost \
           -e VNC_PORT=5900 \
           forumi0721alpinearmhf/alpine-armhf-novnc:latest
```



----------------------------------------
#### Usage

* URL : [http://localhost:6080/](http://localhost:6080/)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 6080/tcp           | Websocket port                                   |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| VNC_HOST           | VNC Host address                                 |
| VNC_PORT           | VNC Port                                         |



----------------------------------------
* [forumi0721alpinex64/alpine-x64-novnc](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-novnc/)
* [forumi0721alpineaarch64/alpine-aarch64-novnc](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-novnc/)
* [forumi0721alpinearmhf/alpine-armhf-novnc](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-novnc/)

